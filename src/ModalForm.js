import React from 'react';
import ModelDiv from './ModelDiv';
const ModalForm=()=>
{
    return(
        <div class="container">
  <h2>Modal Example</h2>
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
  <div class="modal fade" id="myModal" role="dialog">
    <ModelDiv />
  </div>
  
</div>
    )
}
export default ModalForm;