import React from 'react';
import {useState} from 'react';

const Pagination=({postsPerPage,totalPosts,paginate})=>
{
    const pageNumber=[];
    const [current,Setcurrent]=useState(1);
    for(let i=1;i<=Math.ceil(totalPosts/postsPerPage);i++)
    {
        pageNumber.push(i);
    }
    return(
        <div>
            <button onClick={current==1 ? ()=>paginate(1):()=>{Setcurrent(current-1);paginate(current-1);console.log(current-1+"prev")}}>Prev</button>
            {current}
            <button onClick={current==pageNumber.length ? ()=>paginate(pageNumber.length):()=>{Setcurrent(current+1);paginate(current+1);console.log(current+1+"next")}}>Next</button>
        </div>
    )
}
export default Pagination;