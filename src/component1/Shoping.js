import React from 'react';
import {useState} from 'react';
import Pagination from './Pagination'

const Shoping=(props)=>
{
    // const[data2,Setdata2]=useState([{check:false,id:1,item:'Go to supermarket',catagory:'6am'},
    // {check:false,id:2,item:'Go to supermarket',catagory:'Shoping'},
    // {check:false,id:3,item:'Go to supermarket',catagory:'Shoping'},
    // {check:false,id:4,item:'Go to supermarket',catagory:'Shoping'},
    // {check:false,id:5,item:'Go to supermarket',catagory:'Shoping'},
    // {check:false,id:6,item:'Go to supermarket',catagory:'Shoping'},
    // {check:false,id:7,item:'Go to supermarket',catagory:'Shoping'},
    // {check:false,id:8,item:'Go to supermarket',catagory:'Shoping'},
    // {check:false,id:9,item:'Go to supermarket',catagory:'Shoping'} ])

    const data2=props.data2;
    const [currentPage, setCurrentPage]=useState(1);
    const [postsPerPage]=useState(4);
    const indexOflastPost =currentPage*postsPerPage;
    const indexOfFirstPost=indexOflastPost-postsPerPage;
    const currentPosts=data2.slice(indexOfFirstPost,indexOflastPost);
    console.log(currentPosts)
    const paginate=pageNumber=>setCurrentPage(pageNumber);
    return(
        <div>
            <table className="table">
            <thead className="thead-light">
            <tr>
                <th></th>
                <th>No</th>
                <th>Item name</th>
                <th colSpan='3'>Catagory</th>
            </tr>
            </thead>
            <tbody>
            {
            currentPosts.map(value=>{
                return(
                    <tr key={value.id}>
                        <td><input type="checkbox" className="form-check-input" value='' checked={value.check} /></td>
                        <td>{value.id}</td>
                        <td>{value.item}</td>
                        <td>{value.catagory}</td>
                        <td><button type="button" className="btn btn-secondary">Edit</button></td>
                        <td><button type="button" className="btn btn-danger">Delete</button></td>
                    </tr>
    )
})
            }
            </tbody>
            </table>
            <Pagination postsPerPage={postsPerPage}
        totalPosts={data2.length} 
        paginate={paginate} />
        </div>
    )
}
export default Shoping;