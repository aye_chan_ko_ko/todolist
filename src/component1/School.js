import React from 'react';
import {useState} from 'react';
import Pagination from './Pagination';

const School =(props)=>
{
    // const[data1,Setdata1]=useState([{check:false,id:1,item:'Go to school',catagory:'9am'},
    // {check:false,id:2,item:'Go to school',catagory:'School'},
    // {check:false,id:3,item:'Go to school',catagory:'School'},
    // {check:false,id:4,item:'Go to school',catagory:'School'},
    // {check:false,id:5,item:'Go to school',catagory:'School'},
    // {check:false,id:6,item:'Go to school',catagory:'School'},
    // {check:false,id:7,item:'Go to school',catagory:'School'},
    // {check:false,id:8,item:'Go to school',catagory:'School'},
    // {check:false,id:9,item:'Go to school',catagory:'School'} ])
    const data1=props.data1;

    const [currentPage, setCurrentPage]=useState(1);
    const [postsPerPage]=useState(4);
    const indexOflastPost =currentPage*postsPerPage;
    const indexOfFirstPost=indexOflastPost-postsPerPage;
    const currentPosts=data1.slice(indexOfFirstPost,indexOflastPost);
    console.log(currentPosts)
    const paginate=pageNumber=>setCurrentPage(pageNumber);
    //const fun=()=>{data(data1)}

    return(
        <div>
            <table className="table">
                    <thead className="thead-light">
                      <tr>
                        <th></th>
                        <th>No</th>
                        <th>Item name</th>
                        <th colSpan='3'>Catagory</th>
                
                      </tr>
                    </thead>
                    <tbody>
                    {currentPosts.map(value=>{
            return(
            <tr key={value.id}>
                <td><input type="checkbox" className="form-check-input" value='' checked={value.check} /></td>
                <td>{value.id}</td>
                <td>{value.item}</td>
                <td>{value.catagory}</td>
                <td><button type="button" className="btn btn-secondary">Edit</button></td>
                <td><button type="button" className="btn btn-danger">Delete</button></td>
            </tr>
                )
            })}
        </tbody>
    </table>
    <Pagination postsPerPage={postsPerPage}
        totalPosts={data1.length} 
        paginate={paginate} />
</div>
    )
}
export default School;