import React, { Component, useEffect } from "react";
import { useState } from "react";
import AddingForm from "./AddingForm";
import DataShow from "./DataShow";
import CheckUncheck from "./CheckUncheck";
import { findAllByDisplayValue } from "@testing-library/react";
const ViewTable = (props) => {
  let num=0;
  const [data1, Setdata1] = useState([
    { check: false, id: 1, item: "Go to school", catagory: "School" },
    { check: true, id: 2, item: "Go to school", catagory: "School" },
    { check: false, id: 3, item: "Study English", catagory: "School" },
    { check: true, id: 4, item: "Having lunch", catagory: "School" },
    { check: false, id: 5, item: "Exercise", catagory: "School" },
    { check: false, id: 6, item: "Back to Home", catagory: "School" },
    { check: true, id: 7, item: "Go to supermarket", catagory: "Shoping"},
    { check: false, id: 8, item: "playing game", catagory: "Shoping" },
    { check: true, id: 9, item: "Walking to market", catagory: "Shoping" },
    { check: true,id: 10,item: "Searching require tool",catagory: "Shoping"},
    { check: false, id: 11, item: "Eating Chicken", catagory: "Shoping" },
    { check: false, id: 12, item: "Drinking Cola", catagory: "Shoping" },
    { check: true, id: 13, item: "Buying shirt", catagory: "Shoping" },
    { check: false, id: 14, item: "Choosing shoe", catagory: "Shoping" },
    { check: true, id: 15, item: "Back from market", catagory: "Shoping" },
    { check: true, id: 16, item: "Playing Football", catagory: "School" },
    { check: false, id: 17, item: "Relax time", catagory: "School" },
    { check: false, id: 18, item: "Study Mathmatic", catagory: "School" },
    { check: true, id: 19, item: "Relax time", catagory: "Shoping" },
  ]);

  const [selectItem, SetselectItem] = useState("All");
  const [filterValue, SetfilterValue] = useState(data1);
  const [item, SetItem] = useState("");
  const [catagory, Setcatagory] = useState(["School","Shoping"]);
  const [check, Setcheck] = useState(false);
  const [checkList, SetcheckList] = useState(true);
  const [unCheckList, SetunCheckList] = useState(true);
  const [editId, SeteditId] = useState(null);
  const [searchValue,SetsearchValue]=useState('');
  const [singleCatagory,SetsingleCatagory]=useState('');
  const [pagnationError,SetpagnationError]=useState(false);
  const [deleteDivision,SetdeleteDivision]=useState(0);

  // useEffect(()=>
  // {
  //   const deleteData=catagory;
  //   const remove=catagory.map((cat)=>
  //   {const checkResult=data1.filter((fit)=>
  //     { if(fit.catagory==cat)
  //       return fit.catagory})
  //       return checkResult;
  //   //if(checkResult==null) return cat;
  // });
  // const index = remove.findIndex((ind) => ind.length ==0);
  // console.log(remove,"remove data catagory" , index);
  // if(index!=-1)
  // {
  //   deleteData.splice(index,1)
  // }
  // Setcatagory(deleteData)
  // console.log(deleteData,"deltdatadfdkf")
  // },[filterValue])

  //START*******************************FUNCTION FOR ADDING DATA HANDLEING*******************************************
  const itemChange = (e) => {
    SetItem(e.target.value);
  };

  const checkHandle = (e) => {
    Setcheck(e.target.checked);
  };

  const singleCatagoryChange=(e)=>
  {
    SetsingleCatagory(e.target.value);
  }

  // const catagoryChange = (e) => {
  //   Setcatagory(e.target.value);
  // };

  const AddingData = () => {
    let newSingleCatagory=catagory.filter((fit)=>
    {
      return fit.includes(singleCatagory);
    })
    console.log(newSingleCatagory,'newSingleCatagory')
    if(newSingleCatagory.length==0)
    {
      Setcatagory([...catagory,singleCatagory])
    }
   
    if (editId == null) {
      let dataNew = data1;
      if(item=='' && singleCatagory=='')
      {
        alert("please fill want you add data");
      }
      else
      {
        let maxId = dataNew.map((stId) => stId.id).sort((f, l) => l - f);
        let arr = {
          check: check,
          id: maxId[0] + 1,
          item: item,
          catagory: singleCatagory,
        };
        dataNew.push(arr);
  
        {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,selectItem,searchValue)}
        Setdata1(dataNew);
        SetItem("");
        SetsingleCatagory('');
        Setcheck(false);
        SetpagnationError(false);
        alert("success Adding data");
      }
      
    } 
    else {

      // let catagoryEdit=data1.filter((fit)=>
      // {
      //   return(fit.catagory===data1[editId].catagory)
      // })
      // if(catagoryEdit.length==1)
      // {
      //   const index=catagory.findIndex((ind)=>
      //   ind==catagoryEdit[0].catagory
      // );
      // catagory.splice(index,1);
      // Setcatagory([...catagory,singleCatagory]);
      // {CheckUncheck(data1,checkList,unCheckList,SetfilterValue,'All',searchValue)}
      // console.log(catagoryEdit,"EEEEEEEEEEEEEEEEEEEEEEEEE",index, 'ljfldf',catagory)
      // }
      // else
      // {
      //   {CheckUncheck(data1,checkList,unCheckList,SetfilterValue,selectItem,searchValue)}
      // }
      //console.log(catagoryEdit,"EEEEEEEEEEEEEEEEEEEEEEEEE",index, 'ljfldf',catagory)
      data1[editId].item = item;
      data1[editId].catagory = singleCatagory;
      data1[editId].check = check;
      SetItem(""); 
      Setcheck(false);
      SetpagnationError(true);
      SetsingleCatagory('');
      SeteditId(null);
      {CheckUncheck(data1,checkList,unCheckList,SetfilterValue,selectItem,searchValue)}
    }
  };
  //END*******************************FUNCTION FOR ADDING DATA HANDLEING*******************************************

  //START*************************************FUNCTION FOR SELECTITEM HANDLEING ************************************
  const selectItemChange = (e) => {
    let dataNew=data1;
    SetselectItem(e.target.value);
    SetpagnationError(false);
    {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,e.target.value,searchValue)}
  };
  //END*************************************FUNCTION FOR SELECTITEM HANDLEING ************************************

  //START***********************************FUNCTION FOR SEARCH CHANGE HANDLE ************************************
  const searchChange = (e) => {
    const dataNew=data1;
    SetsearchValue(e.target.value)
    SetpagnationError(false);
    // const filteredContacts = data1.filter((contants) => {
    //   return contants.item.toLowerCase().indexOf(e.target.value) !== -1;
    // });
    {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,selectItem,e.target.value)}
  };
  //END***********************************FUNCTION FOR SEARCH CHANGE HANDLE ************************************

  //START*********************************FUNCTION FOR CHECK DATA SHOW HANDLE **********************************
  const showCheckDataHandle = (e) => {
    SetcheckList(e.target.checked);
    const dataNew = data1;
    SetpagnationError(false);
    {CheckUncheck(dataNew,e.target.checked,unCheckList,SetfilterValue,selectItem,searchValue)}
  };
  //START*********************************FUNCTION FOR CHECK DATA SHOW HANDLE **********************************

  //START*********************************FUNCTION FOR UNCHECK DATA SHOW HANDLE **********************************
  const showUnCheckDataHandle = (e) => {
    SetunCheckList(e.target.checked);
    const dataNew = data1;
    SetpagnationError(false);
    {CheckUncheck(dataNew,checkList,e.target.checked,SetfilterValue,selectItem,searchValue)}
  };
  //END*********************************FUNCTION FOR CHECK DATA SHOW HANDLE **********************************

  //START *****************************FUNCTION FOR DATA CHECK HANDLING *************************************
  const checkDataChange = (e, key) => {
    const dataNew = data1;
    const index = dataNew.findIndex((ind) => ind.id == key);
    dataNew[index].check = e.target.checked;
    Setdata1(dataNew);
    SetpagnationError(true);
    {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,selectItem,searchValue)}
  };
  //END *****************************FUNCTION FOR DATA CHECK HANDLING *************************************

  //START ***************************FUNCTION FOR EDIT HANDLING *******************************************
  const editHandler = (key) => {
    const dataNew = data1;
    const index = dataNew.findIndex((ind) => ind.id == key);
    SeteditId(index);
    SetItem(dataNew[index].item);
    SetsingleCatagory(dataNew[index].catagory);
    Setcheck(dataNew[index].check);
    SetpagnationError(true);
  };
  //END ***************************FUNCTION FOR EDIT HANDLING *******************************************

  //START ***************************FUNCTION FOR DELETE HANDLING *******************************************
  const deleteHandler = (key) => {
    const dataNew = data1;
    const index = dataNew.findIndex((ind) => ind.id == key);
    dataNew.splice(index, 1);
    Setdata1(dataNew);
    /////
    const dataSearch1 = dataNew.filter((contants) => {
      return contants.item.toLowerCase().indexOf(searchValue) !== -1;
    });
  // let filterItem1 = dataSearch1.filter((fit) => {
  //     return fit.catagory.indexOf(selectItem) !== -1;
  //   });

  let fiting=catagory.map((cat=>{
    let newest=dataNew.filter((fit)=>
    {
      return fit.catagory.indexOf(cat)!==-1;
    })
    if(newest.length==0)return cat;
    else return null;
  }))
  let fitingfilter=fiting.filter((fit)=>
  {
    return fit!=null;
  })
    ///////
    if(fitingfilter[0]!=null)
    {
      //SetselectItem("All");
      const index=catagory.findIndex((ind)=>
        ind==fitingfilter[0]
      );
      catagory.splice(index,1);
      SetselectItem('All')
      {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,"All",searchValue)}
      SetpagnationError(false);
      
     // console.log("nononononoknonoknoknonnooo",index)
    }
    else
    {
      if(filterValue.length%4==1 && filterValue>4)
      {
        SetdeleteDivision(Math.ceil(filterValue.length/4)-1);
      }
      {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,selectItem,searchValue)}
      SetpagnationError(true);
      
    }
    console.log(fitingfilter,"ghjgjhggghgjhgjhgjhgjhgjhgjhgjhghgjhgh")
    //console.log(deleteDivision,'deleteHandler')
  };
  //END ***************************FUNCTION FOR DELETE HANDLING *******************************************

  return (
    <div className="container">
      <AddingForm
        AddingData={AddingData}
        itemChange={itemChange}
        item={item}
        // catagoryChange={catagoryChange}
        catagory={catagory}
        checkHandle={checkHandle}
        check={check}
        singleCatagoryChange={singleCatagoryChange}
        singleCatagory={singleCatagory}
      />
      <h2>To Do List</h2>
      <div>
        <select
          style={{ width: 20 + "%", height: 30 }}
          value={selectItem}
          onChange={(e) => selectItemChange(e)}
        >
          <option value="All">All</option>
          {
          catagory.map((result)=>{
            num++;
            return(
              <option key={num} value={result}>{result}</option>
            )
          })}
          {/* <option value="School">School</option>
          <option value="Shoping">Shoping</option> */}
          
        </select>
        <input
          type="text"
          onChange={(e) => searchChange(e)}
          style={{ height: 30, width: 30 + "%" }}
        ></input>
        <div>
          <div>
            <input
              type="checkbox"
              className="form-check-input"
              defaultChecked={checkList}
              onClick={(e) => showCheckDataHandle(e)}
            />
            Check
          </div>
          <div>
            <input
              type="checkbox"
              className="form-check-input"
              defaultChecked={unCheckList}
              onChange={(e) => showUnCheckDataHandle(e)}
            />
            UnCheck
          </div>
        </div>
      </div>
      <DataShow
        postData={filterValue}
        editHandler={editHandler}
        deleteHandler={deleteHandler}
        checkDataChange={checkDataChange}
        pagnationError={pagnationError}
        deleteDivision={deleteDivision}
      />
      <div></div>
    </div>
  );
};
export default ViewTable;
