import React, { useState } from "react";

const AddingForm = ({
  AddingData,
  itemChange,
  item,
  check,
  checkHandle,
  singleCatagory,
  singleCatagoryChange,
  errorMessaage1,
  errorMessaage2,
  SeterrorMessage1,
  SeterrorMessage2,
  SetsingleCatagory,
  SetItem,
  Setcheck
}) => {
 const ModalSetting=()=>
 {
  SeterrorMessage1('');
  SeterrorMessage2('');
  SetsingleCatagory('');
  SetItem('');
  Setcheck(false)
 }
  return (
    <div className="modal fade" id="myModal" role="dialog">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title">Adding Data</h4>
        </div>
        <div className="modal-body">
          <form>
            <div className="form-group" style={{ width: 40 + "%" }}>
              <input
                type="text"
                required
                className="form-control"
                placeholder="Enter item"
                value={item}
                name="structure"
                onChange={(e) => itemChange(e)}
              />
              <div style={{ color: "red" }}>{errorMessaage1}</div>
            </div>
            <div className="form-group" style={{ width: 40 + "%" }}>
              <input
                type="text"
                required
                className="form-control"
                placeholder="Enter catagory"
                value={singleCatagory}
                name="structure"
                onChange={(e) => singleCatagoryChange(e)}
              />
              <div style={{ color: "red" }}>{errorMessaage2}</div>
            </div>
            <div className="form-group form-check">
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  type="checkbox"
                  name="remember"
                  checked={check}
                  onChange={(e) => checkHandle(e)}
                />{" "}
                stack
              </label>
            </div>
          </form>
        </div>
        <div className="modal-footer">
        <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={()=>ModalSetting()}>
            Cancel
          </button>
          {(item!='' && singleCatagory!='')?
           <button onClick={() => AddingData(check)} className="btn btn-primary" data-dismiss="modal">
           Add
         </button>:
          <button onClick={() => AddingData(check)} className="btn btn-primary" >
          Add
        </button>}
          {/* <button onClick={() => AddingData(check)} className="btn btn-primary">
            Add
          </button> */}
        </div>
      </div>
    </div>
    </div>
  );
};
export default AddingForm;
