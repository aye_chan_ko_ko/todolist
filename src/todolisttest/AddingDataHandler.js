import CheckUncheck from './CheckUncheck';
const AddingDataHandler=(editId,data1,item,SeterrorMessage1,singleCatagory,SeterrorMessage2,check,
    checkList,unCheckList,SetfilterValue,selectItem,searchValue,Setdata1,
    SetItem,SetsingleCatagory,Setcheck,SetpagnationError,SeteditId,unique,SetselectItem)=>
{
    if (editId == null) {
        let dataNew = data1;
        if(item=='')
        {
          SeterrorMessage1("! Please fill the item")
        }
        else if(singleCatagory=='')
        {
          SeterrorMessage2("! Please fill the catagory")
        }
        else
        {
          
          let maxId = dataNew.map((stId) => stId.id).sort((f, l) => l - f);
          let arr = {
            check: check,
            id: maxId[0] + 1,
            item: item,
            catagory: singleCatagory,
          };
          dataNew.push(arr);
    
          CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,selectItem,searchValue)
          Setdata1(dataNew);
          SetItem("");
          SetsingleCatagory('');
          Setcheck(false);
          SetpagnationError(false);
          alert("success Adding data");
        }      
      } 
      else {
        data1[editId].item = item;
        data1[editId].catagory = singleCatagory;
        data1[editId].check = check;
        SetItem("");
        Setcheck(false);
        SetsingleCatagory('');
        SeteditId(null);
  
        let fiting=unique.map((cat=>{
          let newest=data1.filter((fit)=>
          {
            return fit.catagory===cat;
          })
          if(newest.length==0)return cat;
          else return null;
        })) 
        let fitingfilter=fiting.filter((fit)=>
        {
          return fit!=null;
        })
        if(fitingfilter[0]!=null)
        {
            if(selectItem!='All'){
                SetselectItem('All')
                {CheckUncheck(data1,checkList,unCheckList,SetfilterValue,"All",searchValue)}
                SetpagnationError(false);
                console.log("chainging data")
            }
         
        }
        else{
          SetpagnationError(true);
          {CheckUncheck(data1,checkList,unCheckList,SetfilterValue,selectItem,searchValue)}
          console.log(" Nothing chainging data")
        }
      }
}
export default AddingDataHandler;