import React, { Component, useEffect } from "react";
import { useState } from "react";
import AddingForm from "./AddingForm";
import DataShow from "./DataShow";
import CheckUncheck from "./CheckUncheck";
import DeleteingData from './DeleteingData';
import AddingDataHandler from './AddingDataHandler';
const ViewTable = (props) => {
  let num=0;
  const [data1, Setdata1] = useState([
    { check: false, id: 1, item: "Go to school", catagory: "School" },
    { check: true, id: 2, item: "Go to school", catagory: "School" },
    { check: false, id: 3, item: "Study English", catagory: "School" },
    { check: true, id: 4, item: "Having lunch", catagory: "School" },
    { check: false, id: 5, item: "Exercise", catagory: "School" },
    { check: false, id: 6, item: "Back to Home", catagory: "School" },
    { check: true, id: 7, item: "Go to supermarket", catagory: "Shoping"},
    { check: false, id: 8, item: "playing game", catagory: "Shoping" },
    { check: true, id: 9, item: "Walking to market", catagory: "Shoping" },
    { check: true,id: 10,item: "Searching require tool",catagory: "Shoping"},
    { check: false, id: 11, item: "Eating Chicken", catagory: "Shoping" },
    { check: false, id: 12, item: "Drinking Cola", catagory: "Shoping" },
    { check: true, id: 13, item: "Buying shirt", catagory: "Shoping" },
    { check: false, id: 14, item: "Choosing shoe", catagory: "Shoping" },
    { check: true, id: 15, item: "Back from market", catagory: "Shoping" },
    { check: true, id: 16, item: "Playing Football", catagory: "School" },
    { check: false, id: 17, item: "Relax time", catagory: "School" },
    { check: false, id: 18, item: "Study Mathmatic", catagory: "School" },
    { check: true, id: 19, item: "Relax time", catagory: "Shoping" },
  ]);
  const unique = [...new Set(data1.map(item => item.catagory))];
  const [selectItem, SetselectItem] = useState("All");
  const [filterValue, SetfilterValue] = useState(data1);
  const [item, SetItem] = useState("");
  const [check, Setcheck] = useState(false);
  const [checkList, SetcheckList] = useState(true);
  const [unCheckList, SetunCheckList] = useState(true);
  const [editId, SeteditId] = useState(null);
  const [searchValue,SetsearchValue]=useState('');
  const [singleCatagory,SetsingleCatagory]=useState('');
  const [pagnationError,SetpagnationError]=useState(false);
  const [deleteDivision,SetdeleteDivision]=useState(0);
  const [errorMessage1,SeterrorMessage1]=useState('');
  const [errorMessage2,SeterrorMessage2]=useState('');
  //START*******************************FUNCTION FOR ADDING DATA HANDLEING*******************************************
  const itemChange = (e) => {
    if(e.target.value!='')
    SeterrorMessage1("");
    SetItem(e.target.value);
  };

  const checkHandle = (e) => {
    Setcheck(e.target.checked);
  };

  const singleCatagoryChange=(e)=>
  {
    if(e.target.value!='')
    SeterrorMessage2("");
    SetsingleCatagory(e.target.value);
  }

  const AddingData = () => { 
    AddingDataHandler(editId,data1,item,SeterrorMessage1,singleCatagory,SeterrorMessage2,check,
    checkList,unCheckList,SetfilterValue,selectItem,searchValue,Setdata1,
    SetItem,SetsingleCatagory,Setcheck,SetpagnationError,SeteditId,unique,SetselectItem)  
    // if (editId == null) {
    //   let dataNew = data1;
    //   if(item=='')
    //   {
    //     SeterrorMessage1("! Please fill the item")
    //   }
    //   else if(singleCatagory=='')
    //   {
    //     SeterrorMessage2("! Please fill the catagory")
    //   }
    //   else
    //   {
        
    //     let maxId = dataNew.map((stId) => stId.id).sort((f, l) => l - f);
    //     let arr = {
    //       check: check,
    //       id: maxId[0] + 1,
    //       item: item,
    //       catagory: singleCatagory,
    //     };
    //     dataNew.push(arr);
  
    //     CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,selectItem,searchValue)
    //     Setdata1(dataNew);
    //     SetItem("");
    //     SetsingleCatagory('');
    //     Setcheck(false);
    //     SetpagnationError(false);
    //     alert("success Adding data");
    //   }      
    // } 
    // else {
    //   data1[editId].item = item;
    //   data1[editId].catagory = singleCatagory;
    //   data1[editId].check = check;
    //   SetItem("");
    //   Setcheck(false);
    //   SetsingleCatagory('');
    //   SeteditId(null);

    //   let fiting=unique.map((cat=>{
    //     let newest=data1.filter((fit)=>
    //     {
    //       return fit.catagory===cat;
    //     })
    //     if(newest.length==0)return cat;
    //     else return null;
    //   })) 
    //   let fitingfilter=fiting.filter((fit)=>
    //   {
    //     return fit!=null;
    //   })
    //   if(fitingfilter[0]!=null)
    //   {
    //     SetselectItem('All')
    //     {CheckUncheck(data1,checkList,unCheckList,SetfilterValue,"All",searchValue)}
    //     SetpagnationError(false);
    //   }
    //   else{
    //     SetpagnationError(true);
    //     {CheckUncheck(data1,checkList,unCheckList,SetfilterValue,selectItem,searchValue)}
    //   }
    // }
  };
  //END*******************************FUNCTION FOR ADDING DATA HANDLEING*******************************************

  //START*************************************FUNCTION FOR SELECTITEM HANDLEING ************************************
  const selectItemChange = (e) => {
    let dataNew=data1;
    SetselectItem(e.target.value);
    SetpagnationError(false);
    {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,e.target.value,searchValue)}
  };
  //END*************************************FUNCTION FOR SELECTITEM HANDLEING ************************************

  //START***********************************FUNCTION FOR SEARCH CHANGE HANDLE ************************************
  const searchChange = (e) => {
    const dataNew=data1;
    SetsearchValue(e.target.value)
    SetpagnationError(false);
    {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,selectItem,e.target.value)}
  };
  //END***********************************FUNCTION FOR SEARCH CHANGE HANDLE ************************************

  //START*********************************FUNCTION FOR CHECK DATA SHOW HANDLE **********************************
  const showCheckDataHandle = (e) => {
    SetcheckList(e.target.checked);
    const dataNew = data1;
    SetpagnationError(false);
    {CheckUncheck(dataNew,e.target.checked,unCheckList,SetfilterValue,selectItem,searchValue)}
  };
  //START*********************************FUNCTION FOR CHECK DATA SHOW HANDLE **********************************

  //START*********************************FUNCTION FOR UNCHECK DATA SHOW HANDLE **********************************
  const showUnCheckDataHandle = (e) => {
    SetunCheckList(e.target.checked);
    const dataNew = data1;
    SetpagnationError(false);
    {CheckUncheck(dataNew,checkList,e.target.checked,SetfilterValue,selectItem,searchValue)}
  };
  //END*********************************FUNCTION FOR CHECK DATA SHOW HANDLE **********************************

  //START *****************************FUNCTION FOR DATA CHECK HANDLING *************************************
  const checkDataChange = (e, key) => {
    const dataNew = data1;
    const index = dataNew.findIndex((ind) => ind.id == key);
    dataNew[index].check = e.target.checked;
    Setdata1(dataNew);
    SetpagnationError(true);
    {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,selectItem,searchValue)}
  };
  //END *****************************FUNCTION FOR DATA CHECK HANDLING *************************************

  //START ***************************FUNCTION FOR EDIT HANDLING *******************************************
  const editHandler = (key) => {
    const dataNew = data1;
    const index = dataNew.findIndex((ind) => ind.id == key);
    SeteditId(index);
    SetItem(dataNew[index].item);
    SetsingleCatagory(dataNew[index].catagory);
    Setcheck(dataNew[index].check);
    SetpagnationError(true);
  };
  //END ***************************FUNCTION FOR EDIT HANDLING *******************************************

  //START ***************************FUNCTION FOR DELETE HANDLING *******************************************
  const deleteHandler = (key) => {
    const dataNew = data1;
    DeleteingData(dataNew,Setdata1,unique,SetselectItem,checkList,unCheckList,SetfilterValue,selectItem,searchValue,
      key,filterValue,SetpagnationError,SetdeleteDivision)
  //   const index = dataNew.findIndex((ind) => ind.id == key);
  //   dataNew.splice(index, 1);
  //   Setdata1(dataNew);
  // let fiting=unique.map((cat=>{
  //   let newest=dataNew.filter((fit)=>
  //   {
  //     return fit.catagory===cat;
  //   })
  //   if(newest.length==0)return cat;
  //   else return null;
  // }))
  // let fitingfilter=fiting.filter((fit)=>
  // {
  //   return fit!=null;
  // })
  //   if(fitingfilter[0]!=null)
  //   {
  //     SetselectItem('All')
  //     {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,"All",searchValue)}
  //     SetpagnationError(false);
  //   }
  //   else
  //   {
  //     if(filterValue.length%4==1 && filterValue[filterValue.length-1].id==key)
  //     {
  //       SetdeleteDivision(Math.floor(filterValue.length/4));
  //       console.log(Math.ceil(filterValue.length/4)-1,'deleteHandler')
  //     }
  //     {CheckUncheck(dataNew,checkList,unCheckList,SetfilterValue,selectItem,searchValue)}
  //     SetpagnationError(true);
      
  //   }
    //console.log(fitingfilter,"ghjgjhggghgjhgjhgjhgjhgjhgjhgjhghgjhgh")
    
   
  };
  //END ***************************FUNCTION FOR DELETE HANDLING *******************************************

  return (
    <div className="container">
      <button type="button" className="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add</button>
      <AddingForm
        AddingData={AddingData}
        itemChange={itemChange}
        item={item}
        checkHandle={checkHandle}
        check={check}
        singleCatagoryChange={singleCatagoryChange}
        singleCatagory={singleCatagory}
        errorMessaage1={errorMessage1}
        errorMessaage2={errorMessage2}
        SeterrorMessage1={SeterrorMessage1}
        SeterrorMessage2={SeterrorMessage2}
        SetsingleCatagory={SetsingleCatagory}
        SetItem={SetItem}
        Setcheck={Setcheck}
      />
      <h2>To Do List</h2>
      <div>
        <select
          style={{ width: 20 + "%", height: 30 }}
          value={selectItem}
          onChange={(e) => selectItemChange(e)}
        >
          <option value="All">All</option>
          {
          unique.map((result)=>{
            num++;
            return(
              <option key={num} value={result}>{result}</option>
            )
          })}  
        </select>
        <input
          type="text"
          placeholder="Search"
          onChange={(e) => searchChange(e)}
          style={{ height: 30, width: 30 + "%" }}
        ></input>
        <div>
          <div>
            <input
              type="checkbox"
              className="form-check-input"
              defaultChecked={checkList}
              onClick={(e) => showCheckDataHandle(e)}
            />
            Check
          </div>
          <div>
            <input
              type="checkbox"
              className="form-check-input"
              defaultChecked={unCheckList}
              onChange={(e) => showUnCheckDataHandle(e)}
            />
            UnCheck
          </div>
        </div>
      </div>
      <DataShow
        postData={filterValue}
        editHandler={editHandler}
        deleteHandler={deleteHandler}
        checkDataChange={checkDataChange}
        pagnationError={pagnationError}
        deleteDivision={deleteDivision}
        SetdeleteDivision={SetdeleteDivision}
      />
      <div></div>
    </div>
  );
};
export default ViewTable;
