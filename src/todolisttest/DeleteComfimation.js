import React from 'react';
import { Redirect } from 'react-router-dom';
 const DeleteComfimation =({id,deleteHandler})=>
 {
    return(
    <div className="modal fade" id="myDelete" role="dialog">
        <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h4 className="modal-title">Delete file</h4>
          </div>
          <div className="modal-body">
            <p style={{color:"red",fontSize:30}}> Are you sure want to delete !</p>
          </div>
          <div className="modal-footer">
          <button type="button" className="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="button" className="btn btn-danger" data-dismiss="modal"
             onClick={()=>deleteHandler(id)}>Delete</button>
          </div>
        </div>
        </div>
    </div>
       
    )
 }
 export default DeleteComfimation;