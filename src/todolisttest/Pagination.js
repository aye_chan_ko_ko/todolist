import React, { useEffect } from 'react';
import {useState} from 'react';

const Pagination=({postsPerPage,totalPosts,paginate,deleteDivision,SetdeleteDivision,pagnationError})=>
{
    // useEffect(()=>{
    //     Setcurrent(1)
    // },[totalPosts])
    useEffect(()=>{
        if(pagnationError==false)
        {Setcurrent(1);
        //console.log("paginationErroe")
    }
    if(deleteDivision!=0)
    {
        Setcurrent(deleteDivision);
        SetdeleteDivision(0);
        //console.log(deleteDivision,"deletedivision")
    }
    if(totalPosts<=4)  
    {Setcurrent(1)}  
    },[totalPosts])
    const pageNumber=[];
    const [current,Setcurrent]=useState(1);
    //console.log(pagnationError ,"+++++++++",deleteDivision)
    for(let i=1;i<=Math.ceil(totalPosts/postsPerPage);i++)
    {
        pageNumber.push(i);
    }
    return(
        <div style={{textAlign:"center"}}>
            <button className="btn" onClick={current==1 ? ()=>paginate(1):()=>{Setcurrent(current-1);paginate(current-1);console.log(current-1+"prev")}}>
            <i className='fas fa-chevron-circle-left' style={{fontSize:20,color:"#4A4266"}}></i>
            <i className='fas fa-chevron-circle-left' style={{fontSize:20,color:"#4A4266"}}></i>
            </button>
            <i style={{fontSize:20}}>{current}</i>
            <button className="btn" onClick={current==pageNumber.length ? ()=>paginate(pageNumber.length):()=>{Setcurrent(current+1);paginate(current+1);console.log(current+1+"next")}}>
            <i className='fas fa-chevron-circle-right' style={{fontSize:20,color:"#0AA344"}}></i>
            <i className='fas fa-chevron-circle-right' style={{fontSize:20,color:"#0AA344"}}></i>
            </button>
        </div>        
    )
    
}
export default Pagination;