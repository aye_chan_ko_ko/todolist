import React, { useEffect } from 'react';
import {useState} from 'react';
import Pagination from './Pagination';
import './lineThrough.css';
import DeleteComfimation from './DeleteComfimation';

const DataShow =(props)=>
{
    useEffect(()=>
    {
        if(props.pagnationError==false || props.postData.length<5)
            {setCurrentPage(1);
            console.log("paginationErroe")
        }
        if(props.deleteDivision!=0 && props.postData.length>4 )
        {
        setCurrentPage(props.deleteDivision);
        props.SetdeleteDivision(0);
            console.log(props.deleteDivision,"deletedivision",currentPage)
        }
    },[props.postData])
    //const pginationError=props.pagnationError;
    const [currentPage, setCurrentPage]=useState(1);
    const [postsPerPage]=useState(4);
    const indexOflastPost =currentPage*postsPerPage;
    const indexOfFirstPost=indexOflastPost-postsPerPage;
    const currentPosts=props.postData.slice(indexOfFirstPost,indexOflastPost);
    //console.log(currentPosts , currentPage)
    const paginate=pageNumber=>setCurrentPage(pageNumber);
    const ListNo=((currentPage-1)*4)+1;
    if(props.postData.length!=0)
    {
        return(
            <div>
                <table className="table">
                        <thead className="thead-light">
                          <tr>
                            <th></th>
                            <th>No</th>
                            <th>Item name</th>
                            <th colSpan='4'>Catagory</th>
                    
                          </tr>
                        </thead>
                        <tbody>
                            {/* {console.log('currentPosts',currentPosts)} */}
                        {currentPosts.map((value,index)=>{
                 return(
                <tr key={value.id} className={`${(value.check)? "bgcolor":''}`}>
                    <td><input type="checkbox" className="form-check-input" value='' checked={value.check} onChange={(e)=>props.checkDataChange(e,value.id)}/></td>
                    <td>{ListNo+index}</td>
                    <td className={`${(value.check)? "line":''}`}>{value.item}</td>
                    <td>{value.catagory}</td>
                    <td><button type="button" data-toggle="modal" data-target="#myModal" disabled={(value.check)?true:false} onClick={()=>props.editHandler(value.id)} className="btn">
                    {(value.check)?<i className='far fa-edit' style={{fontSize:20,color:'gray'}}></i>:
                    <i className='far fa-edit' style={{fontSize:20,color:'blue'}}></i>}
                    </button></td>
                    {/* <td><button type="button" className="btn" onClick={()=>props.deleteHandler(value.id)}>
                    <i className='fas fa-trash-alt' style={{fontSize:20,color:'red'}}></i></button></td> */}
                    <td><button type="button"  data-toggle="modal" data-target="#myDelete"
                    className="btn">
                    <i className='fas fa-trash-alt' style={{fontSize:20,color:'red'}}></i></button></td>
                    <td><DeleteComfimation 
                    id={value.id}
                    deleteHandler={props.deleteHandler}/></td>
                </tr>)
                } )}
            </tbody>
        </table>
        <Pagination postsPerPage={postsPerPage}
            totalPosts={props.postData.length} 
            paginate={paginate}
            deleteDivision={props.deleteDivision}
            SetdeleteDivision={props.SetdeleteDivision}
            pagnationError={props.pagnationError}
            
             />
    </div>
        )
    }
    else
    {
        return(<div style={{textAlign:'center '}}>
            <h1>No Data Exist</h1>
        </div>)
        
    }
    
}
export default DataShow;  