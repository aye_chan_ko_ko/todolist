import React, { useState} from "react";

const AddingForm = ({AddingData}) => {

    const[check,Setcheck]=useState(true);
    const[item,SetItem]=useState('');
    const[catagory,Setcatagory]=useState('School');
    
const catagoryChange=(e)=>
{
  Setcatagory(e.target.value)
}
const itemChange=(e)=>
{
  SetItem(e.target.value);
}
const handleChange=(e)=>

{
    Setcheck(e.target.checked);
    console.log(e.target.checked)
}
return (
<div className="container">
  <form>
    <div className="form-group" style={{width:40+'%'}}>
      <input type="text" className="form-control" id="#" placeholder="Enter item" name="structure" onChange={(e)=>itemChange(e)}/>
    </div>
    <div className="form-group" style={{width:40+'%'}}>
      {/* <input type="text" className="form-control" id="#" placeholder="Enter Catagory" name="work"/> */}
      <select style={{width:20+'%',height:30}} value={catagory} onChange={(e)=>catagoryChange(e)}>
                <option value="School">School</option> 
                <option value="Shoping">Shoping</option>
                </select>
    </div> 
    <div className="form-group form-check">
      <label className="form-check-label">
       
        <input className="form-check-input" type="checkbox" name="remember" checked={check} onChange={(e)=>handleChange(e)}  /> stack
      </label>
      
    </div>
   
  </form>
  <button onClick={()=>AddingData(item,catagory,check)} className="btn btn-primary">Add</button>
</div>
      );
    };

    export default AddingForm;