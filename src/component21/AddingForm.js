import React, { useState} from "react";

const AddingForm = ({AddingData , itemChange , item ,catagory ,catagoryChange ,check ,checkHandle}) => {

return (
<div className="container">
  <form>
    <div className="form-group" style={{width:40+'%'}}>
      <input type="text" className="form-control" id="#" placeholder="Enter item"value={item }  name="structure" onChange={(e)=>itemChange(e)}/>
    </div>
    <div className="form-group" style={{width:40+'%'}}>
      <select style={{width:20+'%',height:30}} value={catagory} onChange={(e)=>catagoryChange(e)}>
        <option value="School">School</option> 
        <option value="Shoping">Shoping</option>
      </select>
    </div> 
    <div className="form-group form-check">
      <label className="form-check-label">       
        <input className="form-check-input" type="checkbox" name="remember" defaultChecked={check} onChange={(e)=>checkHandle(e)}  /> stack
      </label>      
    </div>  
  </form>
  <button onClick={()=>AddingData(catagory,check)} className="btn btn-primary">Add</button>
</div>
      );
    };
    export default AddingForm;