import React, { Component, useEffect } from "react";
import { useState } from "react";
import AddingForm from "./AddingForm";
import DataShow from "./DataShow";
const ViewTable = (props) => {
  const [data1, Setdata1] = useState([
    { check: false, id: 1, item: "Go to school", catagory: "School" },
    { check: true, id: 2, item: "Go to school", catagory: "School" },
    { check: false, id: 3, item: "Study English", catagory: "School" },
    { check: true, id: 4, item: "Having lunch", catagory: "School" },
    { check: false, id: 5, item: "Exercise", catagory: "School" },
    { check: false, id: 6, item: "Back to Home", catagory: "School" },
    { check: true, id: 7, item: "Go to supermarket", catagory: "Shoping" },
    { check: false, id: 8, item: "playing game", catagory: "Shoping" },
    { check: true, id: 9, item: "Walking to market", catagory: "Shoping" },
    {
      check: true,
      id: 10,
      item: "Searching require tool",
      catagory: "Shoping",
    },
    { check: false, id: 11, item: "Eating Chicken", catagory: "Shoping" },
    { check: false, id: 12, item: "Drinking Cola", catagory: "Shoping" },
    { check: true, id: 13, item: "Buying shirt", catagory: "Shoping" },
    { check: false, id: 14, item: "Choosing shoe", catagory: "Shoping" },
    { check: true, id: 15, item: "Back from market", catagory: "Shoping" },
    { check: true, id: 16, item: "Playing Football", catagory: "School" },
    { check: false, id: 17, item: "Relax time", catagory: "School" },
    { check: false, id: 18, item: "Study Mathmatic", catagory: "School" },
    { check: true, id: 19, item: "Relax time", catagory: "Shoping" },
  ]);

  const [selectItem, SetselectItem] = useState("All");
  const [filterValue, SetfilterValue] = useState(data1);
  const [item, SetItem] = useState("");
  const [catagory, Setcatagory] = useState("School");
  const [check, Setcheck] = useState(true);
  const [checkList, SetcheckList] = useState(true);
  const [unCheckList, SetunCheckList] = useState(true);
  const [editId, SeteditId] = useState(null);

  const searchChange = (e) => {
    const filteredContacts = data1.filter((contants) => {
      return contants.item.toLowerCase().indexOf(e.target.value) !== -1;
    });
    SetfilterValue(filteredContacts);
  };

  const selectChange = (e) => {
    SetselectItem(e.target.value);
    let filterItem1 = data1.filter((fit) => {
      return fit.catagory.indexOf(e.target.value) !== -1;
    });
    e.target.value == "All"
      ? SetfilterValue(data1)
      : SetfilterValue(filterItem1);
  };

  const deleteHandler = (key) => {
    const dataNew = data1;
    const index = dataNew.findIndex((ind) => ind.id == key);
    dataNew.splice(index, 1);
    Setdata1(dataNew);
    let filterItem = dataNew.filter((fit) => {
      return fit.catagory.indexOf(selectItem) !== -1;
    });
    selectItem == "All" ? SetfilterValue(dataNew) : SetfilterValue(filterItem);
    // console.log(index,key)
  };

  const checkDataChange = (e, key) => {
    const dataNew = data1;
    const index = dataNew.findIndex((ind) => ind.id == key);
    dataNew[index].check = e.target.checked;
    Setdata1(dataNew);
    if (selectItem == "All") {
      SetfilterValue(dataNew);
    } else {
      let filterItem = dataNew.filter((fit) => {
        return fit.catagory.indexOf(selectItem) !== -1;
      });
      SetfilterValue(filterItem);
    }
    // console.log(dataNew)
  };
  const AddingData = () => {
    if (editId == null) {
      let dataNew = data1;
      let maxId = dataNew.map((stId) => stId.id).sort((f, l) => l - f);
      let arr = {
        check: check,
        id: maxId[0] + 1,
        item: item,
        catagory: catagory,
      };
      dataNew.push(arr);
      let filterItem = dataNew.filter((fit) => {
        return fit.catagory.indexOf(selectItem) !== -1;
      });
      selectItem == "All"
        ? SetfilterValue(dataNew)
        : SetfilterValue(filterItem);
      Setdata1(dataNew);
      SetItem("");
      alert("success Adding data");
    } else {
      data1[editId].item = item;
      data1[editId].catagory = catagory;
      data1[editId].check = check;
      SetItem("");
      SeteditId(null);
    }
  };

  const itemChange = (e) => {
    SetItem(e.target.value);
  };

  const checkHandle = (e) => {
    Setcheck(e.target.checked);
    // console.log(e.target.checked)
  };

  const catagoryChange = (e) => {
    Setcatagory(e.target.value);
  };

  const editHandler = (key) => {
    const dataNew = data1;
    const index = dataNew.findIndex((ind) => ind.id == key);
    SeteditId(index);
    SetItem(dataNew[index].item);
    Setcatagory(dataNew[index].catagory);
    Setcheck(dataNew[index].check);
  };

  const showCheckDataHandle = (e) => {};

  const showUnCheckDataHandle = (e) => {};
  return (
    <div className="container">
      <AddingForm
        AddingData={AddingData}
        itemChange={itemChange}
        item={item}
        catagoryChange={catagoryChange}
        catagory={catagory}
        checkHandle={checkHandle}
        check={check}
      />
      <h2>To Do List</h2>
      <div>
        <select
          style={{ width: 20 + "%", height: 30 }}
          value={selectItem}
          onChange={(e) => selectChange(e)}
        >
          <option value="School">School</option>
          <option value="Shoping">Shoping</option>
          <option value="All">All</option>
        </select>
        <input
          type="text"
          onChange={(e) => searchChange(e)}
          style={{ height: 30, width: 30 + "%" }}
        ></input>
        <div>
          <div>
            <input
              type="checkbox"
              className="form-check-input"
              defaultChecked={checkList}
              onChange={(e) => showCheckDataHandle(e)}
            />
            Check
          </div>
          <div>
            <input
              type="checkbox"
              className="form-check-input"
              defaultChecked={unCheckList}
              onChange={(e) => showUnCheckDataHandle(e)}
            />
            UnCheck
          </div>
        </div>
      </div>
      {/* {console.log('datashow')} */}
      <DataShow
        postData={filterValue}
        editHandler={editHandler}
        deleteHandler={deleteHandler}
        checkDataChange={checkDataChange}
      />
      <div></div>
    </div>
  );
};
export default ViewTable;
