import React, { useEffect } from 'react';
import {useState} from 'react';
import Pagination from './Pagination';
import './lineThrough.css';

const DataShow =(props)=>
{
    const [currentPage, setCurrentPage]=useState(1);
    const [postsPerPage]=useState(4);
    const indexOflastPost =currentPage*postsPerPage;
    const indexOfFirstPost=indexOflastPost-postsPerPage;
    const currentPosts=props.postData.slice(indexOfFirstPost,indexOflastPost);
    console.log(currentPosts)
    const paginate=pageNumber=>setCurrentPage(pageNumber);
    return(
        <div>
            <table className="table">
                    <thead className="thead-light">
                      <tr>
                        <th></th>
                        <th>No</th>
                        <th>Item name</th>
                        <th colSpan='3'>Catagory</th>
                
                      </tr>
                    </thead>
                    <tbody>
                        {console.log('currentPosts',currentPosts)}
                    {currentPosts.map(value=>
             
            <tr key={value.id}>
                <td><input type="checkbox" className="form-check-input" value='' defaultChecked={value.check} onChange={(e)=>props.checkDataChange(e,value.id)}/></td>
                <td>{value.id}</td>
                <td className={`${(value.check)? "line":''}`}>{value.item}</td>
                <td>{value.catagory}</td>
                <td><button type="button" disabled={(value.check)?true:false} onClick={()=>props.editHandler(value.id)} className="btn btn-secondary">Edit</button></td>
                <td><button type="button" className="btn btn-danger" onClick={()=>props.deleteHandler(value.id)}>Delete</button></td>
            </tr>
                
            )}
        </tbody>
    </table>
    <Pagination postsPerPage={postsPerPage}
        totalPosts={props.postData.length} 
        paginate={paginate} />
</div>
    )
}
export default DataShow;  